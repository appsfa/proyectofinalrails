class AddTestGradeToStudent < ActiveRecord::Migration[5.2]
  def change
    add_column :students, :test_grade, :integer
  end
end
