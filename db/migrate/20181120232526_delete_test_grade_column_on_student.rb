class DeleteTestGradeColumnOnStudent < ActiveRecord::Migration[5.2]
  def change
    remove_column :students, :testGrade
  end
end
