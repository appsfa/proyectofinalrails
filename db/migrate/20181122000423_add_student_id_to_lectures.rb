class AddStudentIdToLectures < ActiveRecord::Migration[5.2]
  def change
    add_column :lectures, :student_id, :integer
  end
end
