class AddContractDueDateToContracts < ActiveRecord::Migration[5.2]
  def change
    add_column :contracts, :contract_due_date, :date
  end
end
